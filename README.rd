= Apache Arrow - データ処理ツールの次世代プラットフォーム

みなさんはApache Arrowを知っていますか？ 普段データを処理している人でも今はまだ知らない人の方が多いかもしれません。しかし、数年後には「データ処理をしている人ならほとんどの人が知っている」となるプロダクトです。（そうなるはずです。）

Apache Arrowはメモリー上でデータ処理するときに必要なもの一式を提供します。たとえば、効率的なデータ交換のためのデータフォーマット、CPU/GPUの機能を活用した高速なデータ操作機能などです。

一部のデータ処理ツールではすでにApache Arrowを使い始めています。たとえば、Apache SparkはApache Arrowを活用することでPySpark（PythonからApache Sparkを使うためのモジュール）とのやりとりを高速化しています。データ量によっては10倍以上も高速になります。（リンク先の例では20秒→0.7秒と約30倍高速になっています。）

この講演ではApache Arrowの概要だけでなく最新情報も紹介します。この講演を聞くことでApache Arrowのことを網羅的に把握できます。

Apache Arrowはデータ処理ツールが共通で必要なもの一式を提供するので、より多くのツールがApache Arrowを活用し、より多くの人がApache Arrowの開発に参加すると、より多くの人が豊かになります。Apache ArrowはOSSなのでだれでも自由に活用したり開発に参加したりできます。Apache Arrowのことを知ってOSSならではの「共有するほど豊かになる」アプローチに参加しましょう！

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者名は以下の通りです。

  * 須藤功平（またはKouhei Sutou）

=== 画像

==== クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-oss-forum-apache-arrow

=== 表示

  rabbit rabbit-slide-kou-oss-forum-apache-arrow.gem

